#! /bin/bash
set -e

export PIGLIT_PLATFORM=gbm

source `dirname $0`/mesa-build-helper.sh

if [ $# -ne 2 ]; then
    echo "Usage: shader-db-runner.sh mesa_refspec1 mesa_refspec2"
    exit 1
fi

SHADER_DB_GIT_PATH="${PIGLIT_RUNNER_PATH}/shader-db"
SHADER_DB_CACHE_PATH="${PIGLIT_RUNNER_CACHE_PATH}/shader-db"

function run_shader-db {
    MESA_REF=$1
    build_mesa

    RESULT_HASH=`sha1_hash ${MESA_HASH} ${SHADER_DB_HASH}`
    SHADER_DB_RESULT="${SHADER_DB_CACHE_PATH}/${RESULT_HASH}.log"
    if [ -f "${SHADER_DB_RESULT}" ]; then
        echo "Shader-db already run for hash ${SHADER_DB_HASH}.  Skipping..."
    else
        cd ${SHADER_DB_GIT_PATH}
        mkdir -p ${SHADER_DB_CACHE_PATH}
        ./run.py | tee ${SHADER_DB_RESULT}.tmp
        mv ${SHADER_DB_RESULT}.tmp ${SHADER_DB_RESULT}
    fi
}

# Checkout shader-db
cd ${SHADER_DB_GIT_PATH}
SHADER_DB_HASH=`${GIT_CMD} rev-parse ${SHADER_DB_REF}`
if [ -d "${SHADER_DB_GIT_PATH}/shaders/non-free" ]; then
    cd ${SHADER_DB_GIT_PATH}/shaders/non-free
    NON_FREE_HASH=`${GIT_CMD} rev-parse ${SHADER_DB_REF}`
    SHADER_DB_HASH=`sha1_hash ${SHADER_DB_HASH} ${NON_FREE_HASH}`
    cd ${SHADER_DB_GIT_PATH}
fi

$GIT_CMD checkout --detach ${SHADER_DB_REF}

run_shader-db $1
FIRST_RESULT=${SHADER_DB_RESULT}
run_shader-db $2
SECOND_RESULT=${SHADER_DB_RESULT}

cd ${SHADER_DB_GIT_PATH}
./report.py ${FIRST_RESULT} ${SECOND_RESULT}
