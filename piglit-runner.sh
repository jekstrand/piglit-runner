#! /bin/bash
set -e

UPLOAD_PATH="jekstrand@people.freedesktop.org:public_html/piglit"
PIGLIT_ARGS="quick.py -x glx -x glean"
PIGLIT_ARGS+=" -c"
#PIGLIT_ARGS+=" -1 --dmesg"
PIGLIT_PLATFORM=gbm

############################################################################
# You should not need to edit the script beyond this point.
############################################################################

function upload_results {
    rsync -avz ${SUMMARY_PATH} ${UPLOAD_PATH}
}

if [ $1 = "--upload-only" ]; then
    upload_results
    exit 0
fi

source `dirname $0`/mesa-build-helper.sh

if [ $# -lt 2 ]; then
    echo "Usage: piglit-runner.sh piglit_refspec mesa_refspec1 [mesa_refspec2 ...]"
    exit 1
fi

PIGLIT_GIT_PATH="${PIGLIT_RUNNER_PATH}/piglit"
PIGLIT_BUILD_PATH="${PIGLIT_RUNNER_PATH}/piglit-build"

RESULTS_PATH="${PIGLIT_RUNNER_CACHE_PATH}/results"
SUMMARY_PATH="${PIGLIT_RUNNER_CACHE_PATH}/summary"

function build_piglit {
    cd "${PIGLIT_GIT_PATH}"
    git_fetch_ref ${PIGLIT_REF}
    PIGLIT_HASH=`${GIT_CMD} rev-parse ${PIGLIT_REF}`
    mkdir -p "${PIGLIT_BUILD_PATH}"
    GIT_WORK_TREE="${PIGLIT_BUILD_PATH}" $GIT_CMD checkout -f --detach ${PIGLIT_REF}
    cd "${PIGLIT_BUILD_PATH}"
    cmake -G Ninja .
    ninja-build
}

function run_piglit {
    RESULT_HASH=`sha1_hash ${MESA_HASH} ${PIGLIT_HASH} ${PIGLIT_ARGS}`
    PIGLIT_TMP_RESULT="${WORK_PATH}/results/${RESULT_HASH}.tmp"
    PIGLIT_RESULT="${RESULTS_PATH}/${RESULT_HASH}.json.bz2"

    if [ -f "${PIGLIT_RESULT}" ]; then
        echo "Piglit already run for piglit ${PIGLIT_HASH} and mesa ${MESA_HASH}."
        echo "Skipping ${RESULT_HASH} ..."
    else
        cd "${PIGLIT_BUILD_PATH}"
        export PIGLIT_PLATFORM

        ./piglit run $PIGLIT_ARGS -n "${MESA_REF}" "${PIGLIT_TMP_RESULT}"
        mv "${PIGLIT_TMP_RESULT}/results.json.bz2" "${PIGLIT_RESULT}"
    fi
}

# Now that everything is set up, we can actually build piglit, build a few
# revisions of mesa, and run some tests!
PIGLIT_REF="$1"
build_piglit

RESULTS_DIRS=""
while [ $# -ge 2 ]; do
    MESA_REF="$2"
    build_mesa
    run_piglit
    RESULTS_DIRS+=" ${PIGLIT_RESULT}"
    shift
done
rm -rf ${SUMMARY_PATH}
echo "Generating results file (this may take a minute)..."
${PIGLIT_BUILD_PATH}/piglit-summary-html.py ${SUMMARY_PATH} $RESULTS_DIRS

# Now that piglit is 100% finished, go ahead and upload.
if [ -n ${UPLOAD_PATH} ]; then
    echo "Piglit runs finished.  Press ENTER to upload."
    read -s
    upload_results
fi
