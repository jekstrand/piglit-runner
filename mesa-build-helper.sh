MESA_AM_CONFIG_PARAMS="--enable-gles2 --disable-gallium-egl
--with-egl-platforms=x11,wayland,drm --enable-gbm
--enable-dri3 --enable-debug"
MESA_MESON_CONFIG_PARAMS="-Dplatforms=x11,wayland,drm
-Dbuildtype=debugoptimized"
DRI_DRIVERS="i965"
GALLIUM_DRIVERS=""
USE_GALLIUM="no"

USE_MESON="yes"

GIT_CMD="git"
MAKE_CMD="make -j16"
MESON_CMD="meson"
NINJA_CMD="ninja"
WORK_PATH="/tmp/piglit-runner-${USER}"
EXTRA_LIBS_PATH="${HOME}/.local/lib"

############################################################################
# You should not need to edit the script beyond this point.
############################################################################

cd `dirname $0`
PIGLIT_RUNNER_PATH=`pwd`

PIGLIT_RUNNER_CACHE_PATH="${PIGLIT_RUNNER_PATH}/cache"

MESA_GIT_PATH="${PIGLIT_RUNNER_PATH}/mesa"
MESA_CACHE_PATH="${PIGLIT_RUNNER_CACHE_PATH}/mesa-build"
MESA_BUILD_PATH="${WORK_PATH}/$$/mesa-build"

# Probably either lib or lib64
if [ "${USE_MESON}" = "yes" ]; then
    MESA_LIB_SUFFIX="lib64"
else
    MESA_LIB_SUFFIX="lib"
fi

function sha1_hash {
    echo $* | sha1sum - | cut -d\  -f1
}

function git_fetch_ref {
    if echo "$1" | grep -q '/'; then
        # This is a branch name, not a SHA1 hash
        REMOTE=`echo "$1" | cut -d/ -f1`
        echo Fetching $REMOTE
        $GIT_CMD fetch ${REMOTE}
    else
        $GIT_CMD fetch --all
    fi
}


function build_mesa {
    cd "${MESA_GIT_PATH}"
    git_fetch_ref ${MESA_REF}
    MESA_HASH=`${GIT_CMD} rev-parse ${MESA_REF}`
    MESA_HASH=`sha1_hash ${MESA_HASH} ${MESA_AM_CONFIG_PARAMS}`
    MESA_HASH=`sha1_hash ${MESA_HASH} ${MESA_MESON_CONFIG_PARAMS}`

    MESA_PREFIX="${MESA_CACHE_PATH}/${MESA_HASH}"
    if [ -d "${MESA_PREFIX}" ]; then
        echo "Mesa hash ${MESA_HASH} already built.  Skipping..."
    else
        ${GIT_CMD} clean -xfd
        ${GIT_CMD} checkout --detach ${MESA_REF}

        # Instead of working on the directory directly, we work on a
        # temporary directory.  This way, if something fails, we get a
        # rebuild instead of keeping a bad build in the cache.
        rm -rf "${MESA_BUILD_PATH}/${MESA_HASH}.tmp"
        mkdir -p "${MESA_BUILD_PATH}/${MESA_HASH}.tmp"

        export "PKG_CONFIG_PATH=${EXTRA_LIBS_PATH}/pkgconfig"
        cd "${MESA_BUILD_PATH}/${MESA_HASH}.tmp"
        if [ "${USE_MESON}" = "yes" ]; then
            ${MESON_CMD} --prefix=${MESA_PREFIX} \
                         $MESA_MESON_CONFIG_PARAMS \
                         ${MESA_GIT_PATH}
            ${NINJA_CMD}

            # Everything built ok.  Move the library folder from the temporary
            # build directory to the final locaiton in the cache
            mkdir -p "${MESA_PREFIX}"
            ${NINJA_CMD} install
        else
            "${MESA_GIT_PATH}/autogen.sh" --prefix="${MESA_PREFIX}" \
                                          $MESA_AM_CONFIG_PARAMS
            $MAKE_CMD

            # Everything built ok.  Move the library folder from the temporary
            # build directory to the final locaiton in the cache
            mkdir -p "${MESA_PREFIX}"
            make install
        fi
        cd "${MESA_BUILD_PATH}"
        rm -rd ${MESA_HASH}.tmp
    fi

    # Set up environment
    MESA_LIB_PATH="${MESA_PREFIX}/${MESA_LIB_SUFFIX}"
    export LD_LIBRARY_PATH="${MESA_LIB_PATH}:${EXTRA_LIBS_PATH}:${MESA_LD_LIBRARY_PATH_BACKUP}"
    export LIBGL_DRIVERS_PATH="${MESA_LIB_PATH}/dri"
    export GBM_DRIVERS_PATH="${LIBGL_DRIVERS_PATH}"
}

while [ $# -ge 1 ]; do
    case $1 in
    --target=*)
        target=`echo $1 | sed -e 's/^--target=//'`
        source ${PIGLIT_RUNNER_PATH}/targets/${target}.sh
        ;;
    *)
        break
        ;;
    esac
    shift
done

if [ $# -lt 2 ]; then
    echo "Usage: piglit-runner.sh piglit_refspec mesa_refspec1 [mesa_refspec2 ...]"
    exit 1
fi

# Now that we have our target and other configuration information figured
# out, we can go ahead and build our environment.
MESA_AM_CONFIG_PARAMS+=" --with-dri-drivers=${DRI_DRIVERS}"
MESA_AM_CONFIG_PARAMS+=" --with-gallium-drivers=${GALLIUM_DRIVERS}"
MESA_MESON_CONFIG_PARAMS+=" -Ddri-drivers=${DRI_DRIVERS}"
MESA_MESON_CONFIG_PARAMS+=" -Dgallium-drivers=${GALLIUM_DRIVERS}"
MESA_MESON_CONFIG_PARAMS+=" -Dvulkan-drivers="
# We're going to overwrite this several times; keep a backup.
MESA_LD_LIBRARY_PATH_BACKUP="${LD_LIBRARY_PATH}"

